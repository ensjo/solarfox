	.module mc1000

	.area _CODE

	MC1000_COL32 = 0x80
	MC1000_MODBUF = 0xf5
	MC1000_VRAM = 0x8000
	MC1000_COL32_MASCARA_VRAM_INV = 0b11111110
	MC1000_COL32_VRAM_INATIVA     = 0b00000001

; Preenche 6 KiB de VRAM com um byte.
; O registrador H contém o valor a ser usado na porta COL32 (0x80) para
; habilitar a VRAM.
; O registrador L contém o valor com o qual a VRAM será preenchida.
_preencheVRAM::
	; Salva registradores.
	push af
	push hl
_preencheVRAM0:
	push de
	push bc
	
	; Habilita VRAM.
	ld a,h
	and a,#MC1000_COL32_MASCARA_VRAM_INV
	out (#MC1000_COL32),a
	
	; Preenche VRAM com o byte de cor em L.
	ld b,l
	ld hl,#MC1000_VRAM ; HL = endereço do primeiro byte da VRAM.
	ld (hl),b ; Coloca valor a preencher no primeiro byte da VRAM.
	ld d,h
	ld e,l
	inc de ; DE = endereço do segundo byte da VRAM.
	ld bc,#6 * 1024 - 1 ; BC = quantidade de bytes a copiar (6 KiB - 1).
	ldir
	
	; Desabilita VRAM.
	or a,#MC1000_COL32_VRAM_INATIVA
	out (#MC1000_COL32),a
	
	; Restaura registradores.
	pop bc
	pop de
	pop hl
	pop af
	
	ret

; Preenche 6 KiB de VRAM com um byte.
; O registrador A contém o valor com o qual a VRAM será preenchida.
; A VRAM será habilitada com o valor armazenado na variável MODBUF.
_preencheVRAMEmModbuf::
	; Salva registradores.
	push af
	push hl
	
	ld l,a
	ld a,(#MC1000_MODBUF)
	ld h,a
	
	jr _preencheVRAM0


; Para debugar.

_printHexInt::
	ld b,#4
	jr _printHexChar0

_printHexChar::
	ld h,a
	ld l,#0
	ld b,#2
_printHexChar0:
	xor a
	add hl,hl
	rla
	add hl,hl
	rla
	add hl,hl
	rla
	add hl,hl
	rla
	add a,#'0'
	cp	a,#'9'+1
	jr c,_printHexChar1
	add a,#'A'-('9'+1)
_printHexChar1:
	ld c,a
	call _MC1000_CO ; Imprime caracter no registrador C.
	djnz _printHexChar0
	ret

;

	_print == 0xc0de
