	; crt0.s for CCE MC-1000. By Emerson José Silveira da Costa, 2017-11-10.

	.module	crt0
	.globl	_main
	.globl	l__INITIALIZER
	.globl	s__INITIALIZED
	.globl	s__INITIALIZER

	; ==================================

	; MODELO PARA PROGRAMA EM CÓDIGO DE MÁQUINA
	; CARREGÁVEL NO MC-1000 VIA COMANDO LOAD.

	.area	_HEADER (ABS)

	; ==================================

	; (1) Programa em BASIC que chama a porção em linguagem
	; de máquina residente nos bytes após o fim do programa:

	; 1  CALL 992

	.org	0x03d5

	.dw	endlinha2 ; Endereço do próximo registro de linha do programa em BASIC.
	.dw	1 ; Número da linha do programa em BASIC.
	.db	0xa2 ; Token de "CALL".
	.ascii	"992"
	.db	0 ; Fim da linha.
endlinha2:
	.dw	0 ; Endereço do próximo registro de linha = 0, indicando fim do programa.

	; ==================================

	; (2) Início do programa em linguagem de máquina.
	; Faz preparativos antes de pular para a função main().

	; .org	0x03e0 ; =992.

	; Reativa a impressão de caracteres que é desativada
	; quando um programa BASIC sem nome (autoexecutável)
	; é carregado.
	xor	a
	ld	(0x0344),a

	; Inicializa variáveis globais.
	call	gsinit

	; Executa main() (estará na área _CODE).
	jp	_main
	
	.area	_CODE

	; .org	0x03ea ; =1002.	

	; Neste ponto estamos no endereço 0x3ea,
	; endereço que deve ser usado na opção
	; --code-loc do linkador.
	; Aqui se iniciará o código do programa em C.
	
	; ==================================

	; Declara antecipadamente demais áreas usadas pelo SDCC.
	; As áreas são criadas à medida que são declaradas, então
	; isto define a ordem em que as áreas aparecerão no
	; código final.
	
	; Neste ponto estamos no endereço 0x3ea,
	; endereço que deve ser usado 


	.area	_HOME
	.area	_INITIALIZER
	.area	_GSINIT
	.area	_GSFINAL

	.area	_DATA
	.area	_INITIALIZED
	.area	_BSEG
	.area	_BSS
	.area	_HEAP
	
	; ==================================

	; Inicialização de variáveis.

	.area	_GSINIT
gsinit::
	ld	bc, #l__INITIALIZER
	ld	a, b
	or	a, c
	jr	Z, gsinit_next
	ld	de, #s__INITIALIZED
	ld	hl, #s__INITIALIZER
	ldir
gsinit_next:

	.area	_GSFINAL
	ret
