// PORTAS DE E/S DO MC1000.
// Vide https://sites.google.com/site/ccemc1000/sistema/portas

__sfr __at 0x04 MC1000_SPORT;
__sfr __at 0x04 MC1000_STROB;
__sfr __at 0x05 MC1000_DPORT;
__sfr __at 0x10 MC1000_RPORT1;
__sfr __at 0x11 MC1000_DPORT1;
__sfr __at 0x12 MC1000_COL80;
__sfr __at 0x20 MC1000_REG;
__sfr __at 0x40 MC1000_RD;
__sfr __at 0x60 MC1000_WR;
__sfr __at 0x80 MC1000_COL32;


// ----------------------------------------------------------------------------

// VARIÁVEIS DO SISTEMA DO MC1000.
// Vide https://sites.google.com/site/ccemc1000/sistema/variaveis-do-sistema

typedef union {
	unsigned char bytes[3];
	struct {
		unsigned char opcode;
		unsigned int address;
	};
} MC1000_hook_t;

typedef struct {
	unsigned char scoreA;
	unsigned char scoreB;
} MC1000_score_t;


// 0x000 ~ 0x00ff : BUFFER DE EXECUÇÃO DO PROGRAMA DEBUG:


__at 0x0003 unsigned char MC1000_IOBYTE;
__at 0x000f unsigned char MC1000_C40_80; // "C40?80"
__at 0x0038 MC1000_hook_t MC1000_RST_38;
__at 0x00f5 unsigned char MC1000_MODBUF;
__at 0x00f6 unsigned char MC1000_CLR;
__at 0x00f7 unsigned int MC1000_UPDBM;
__at 0x00f9 unsigned int MC1000_UPDBCM;
__at 0x00fb unsigned int MC1000_STAR;
__at 0x00fd unsigned int MC1000_ENDT;
__at 0x00ff unsigned int MC1000_BORDER;


// 0x0100 ~ 0x01ff : BUFFER DE EXECUÇÃO DO MONITOR:


__at 0x0103 unsigned char MC1000_PGM;
__at 0x0103 unsigned char MC1000_PGN;
__at 0x0103 unsigned char MC1000_MUSIC;
__at 0x0104 unsigned char MC1000_PLAY;
__at 0x0105 unsigned char MC1000_PLAYMX;
__at 0x0106 unsigned char MC1000_HEAD;
__at 0x0107 unsigned int MC1000_RANDOM;
__at 0x0109 MC1000_score_t* MC1000_RCRDPT;
__at 0x010b MC1000_score_t MC1000_RECORD[8];
__at 0x0113 unsigned int MC1000_DLNG;
__at 0x0115 unsigned char MC1000_YCORD;
__at 0x0116 unsigned char MC1000_XCORD;
__at 0x011b unsigned char MC1000_KEY0[4];
__at 0x0120 MC1000_hook_t MC1000_JOB;
__at 0x0123 unsigned char MC1000_SCOREA;
__at 0x0124 unsigned char MC1000_SCOREB;
__at 0x0125 unsigned char MC1000_SHAPE0;
__at 0x0126 unsigned char MC1000_KTIME;
__at 0x0128 unsigned char MC1000_PNTR;
__at 0x012d unsigned char MC1000_COSW;
__at 0x012e unsigned char MC1000_KEYSW;
__at 0x012f unsigned char MC1000_FSHCNT;
__at 0x0130 MC1000_hook_t MC1000_JOBM;
__at 0x0133 unsigned char MC1000_FLASHB;
__at 0x0135 unsigned char MC1000_TABLE;
__at 0x0135 unsigned char MC1000_NSA;
__at 0x0137 unsigned char* MC1000_NAA;
__at 0x0139 unsigned char MC1000_AVALUE;
__at 0x013a unsigned char MC1000_VOICEA;
__at 0x013b unsigned char MC1000_INTRPA;
__at 0x013c unsigned char MC1000_TEMPA;
__at 0x013d unsigned char MC1000_INTA;
__at 0x013e unsigned char MC1000_NSB;
__at 0x0140 unsigned char* MC1000_NBB;
__at 0x0142 unsigned char MC1000_BVALUE;
__at 0x0143 unsigned char MC1000_VOICEB;
__at 0x0144 unsigned char MC1000_INTRPB;
__at 0x0145 unsigned char MC1000_TEMPB;
__at 0x0146 unsigned char MC1000_INTB;
__at 0x0147 unsigned char MC1000_NSC;
__at 0x0149 unsigned char* MC1000_NCC;
__at 0x014b unsigned char MC1000_CVALUE;
__at 0x014c unsigned char MC1000_VOICEC;
__at 0x014d unsigned char MC1000_INTRPC;
__at 0x014e unsigned char MC1000_TEMPC;
__at 0x014f unsigned char MC1000_INTC;
__at 0x0150 unsigned char MC1000_ENABLE;
__at 0x0151 unsigned char MC1000_AMPLIT;
__at 0x0152 unsigned char MC1000_REGIST[2];
__at 0x0154 unsigned char MC1000_DEFIN;
__at 0x0155 unsigned char MC1000_ONAMP[2];
__at 0x0157 unsigned char MC1000_LPLAY;
__at 0x0158 unsigned char MC1000_MODEK;
__at 0x0159 unsigned char* MC1000_LNHD;
__at 0x015b unsigned char* MC1000_SNPTR;
__at 0x015d unsigned char MC1000_LCNT;
__at 0x015e unsigned char MC1000_CHECK;
__at 0x0163 unsigned char* MC1000_DSNAM;
__at 0x0165 unsigned char* MC1000_DENAM;
__at 0x0167 unsigned char MC1000_HISCOR;
__at 0x0169 unsigned char MC1000_TEMP;
__at 0x016c unsigned char MC1000_RIGHTJ;
__at 0x016d unsigned char MC1000_CHANA[5];
__at 0x0172 unsigned char MC1000_TONEA;
__at 0x0173 unsigned char MC1000_CHANB[5];
__at 0x0178 unsigned char MC1000_TONEB;
__at 0x0179 unsigned char MC1000_CHANC[5];
__at 0x017e unsigned char MC1000_TONEC;
__at 0x017f unsigned char MC1000_OBUF[14];
__at 0x018d unsigned char MC1000_FILNAM[14];


// 0x0200 ~ 0x02ff : BUFFER DE LINHA:


__at 0x017f unsigned char MC1000_INPBUF[14];


// 0x0300 ~ 0x3d4 : BUFFER DE INTERPRETAÇÃO DO PROGRAMA BASIC.

// ----------------------------------------------------------------------------

// OUTRAS ÁREAS DA MEMÓRIA:

// 0X8000 ~ 0x9fff : 8 KiB de VRAM.
// O MC6847 só acessa 6KiB de VRAM, mas o hardware do MC1000
// comuta um banco de 8 KiB.

__at 0x8000 unsigned char MC1000_VRAM[8 * 1024]; // Banco de VRAM pode ter até 8 KiB.


// ----------------------------------------------------------------------------

// ROTINAS DA ROM:

// [Ainda é preciso escolher a melhor forma de disponibilizar as rotinas da ROM
// para o C. Abaixo há duas soluções.]
// [Com __z88dk_fastcall pode-se passar 1 parâmetro de até 32 bits em registradores
// em vez de usar a pilha do Z80: 8 bits em L, 16 bits em HL, 32 bits em DEHL.]

static void fake_function() __naked {
	__asm
		_MC1000_ST     == 0xc000
		_MC1000_BAENT0 == 0xc003
		_MC1000_KEY    == 0Xc006
		_MC1000_KEY_   == 0xc009
		_MC1000_CO     == 0xc00c
		_MC1000_TAPIN  == 0xc00f
		_MC1000_TAPOUT == 0xc012
		_MC1000_GET1   == 0Xc015
	__endasm;
}

extern void MC1000_ST();
extern void MC1000_BAENT0();
extern unsigned char MC1000_KEY() __naked __z88dk_fastcall;
extern unsigned char MC1000_KEY_() __naked __z88dk_fastcall; // "KEY?"
extern void MC1000_CO();
extern void MC1000_TAPIN();
extern void MC1000_TAPOUT();
extern void MC1000_GET1();

#define MC1000_MSG() (* (func_ptr_t) 0xc018) ()
#define MC1000_TLOAD() (* (func_ptr_t) 0xc01e) ()
#define MC1000_GETL() (* (func_ptr_t) 0xc021) ()
#define MC1000_ISCN() (* (func_ptr_t) 0xc024) ()
#define MC1000_INTRUP() (* (func_ptr_t) 0xc027) ()
#define MC1000_MPY() (* (func_ptr_t) 0xc02a) ()
#define MC1000_DIV() (* (func_ptr_t) 0xc02d) ()
#define MC1000_XCLEAR() (* (func_ptr_t) 0xc030) ()
#define MC1000_XCLR1() (* (func_ptr_t) 0xc033) ()
#define MC1000_D4X5() (* (func_ptr_t) 0xc036) ()
#define MC1000_TOP() (* (func_ptr_t) 0xc039) ()
#define MC1000_PLAYNO() (* (func_ptr_t) 0xc03c) ()
#define MC1000_DISPY2() (* (func_ptr_t) 0xc03f) ()
#define MC1000_SHOWNO() (* (func_ptr_t) 0xc042) ()
#define MC1000_NEXTGM() (* (func_ptr_t) 0xc045) ()
#define MC1000_DELAYB() (* (func_ptr_t) 0xc048) ()
#define MC1000_SCORE() (* (func_ptr_t) 0xc04b) ()
#define MC1000_LSCORE() (* (func_ptr_t) 0xc04e) ()
#define MC1000_SHAPON() (* (func_ptr_t) 0xc051) ()
#define MC1000_SHAPOF() (* (func_ptr_t) 0xc054) ()
#define MC1000_DISPY() (* (func_ptr_t) 0xc057) ()
#define MC1000_LPDRV() (* (func_ptr_t) 0xc05a) ()
#define MC1000_LPSTS() (* (func_ptr_t) 0xc05d) ()
#define MC1000_BEEP() (* (func_ptr_t) 0xc060) ()


// ----------------------------------------------------------------------------


// Valores para a porta COL32 (0x80) e a variável MODBUF (0x00f5)
// a serem combinados (com OR).

// a) Modos de vídeo.
#define MC1000_COL32_MASCARA_MODO     0b11111100
#define MC1000_COL32_MASCARA_MODO_INV 0b00000011

#define MC1000_COL32_ALPHA 0b00000000
#define MC1000_COL32_EXT   0b00100000
#define MC1000_COL32_SG4   0b01000000
#define MC1000_COL32_SG6   0b01100000
#define MC1000_COL32_CG1   0b10000000
#define MC1000_COL32_RG1   0b10000100
#define MC1000_COL32_CG2   0b10001000
#define MC1000_COL32_RG2   0b10001100
#define MC1000_COL32_CG3   0b10010000
#define MC1000_COL32_RG3   0b10010100
#define MC1000_COL32_CG6   0b10011000
#define MC1000_COL32_RG6   0b10011100

// b) Paletas.
#define MC1000_COL32_MASCARA_CSS     0b00000010
#define MC1000_COL32_MASCARA_CSS_INV 0b11111101

#define MC1000_COL32_CSS0  0b00000000
#define MC1000_COL32_CSS1  0b00000010

// c) Ativação do banco de VRAM.
#define MC1000_COL32_MASCARA_VRAM     0b00000001
#define MC1000_COL32_MASCARA_VRAM_INV 0b11111110

#define MC1000_COL32_VRAM_ATIVA   0b00000000
#define MC1000_COL32_VRAM_INATIVA 0b00000001


// ----------------------------------------------------------------------------

// Cores para os modos gráficos.

#define MC1000_SG4_MASCARA_COR   0b01110000
#define MC1000_SG4_MASCARA_BLOCO 0b00001111

#define MC1000_SG4_VERDE    0b00000000
#define MC1000_SG4_AMARELO  0b00010000
#define MC1000_SG4_AZUL     0b00100000
#define MC1000_SG4_VERMELHO 0b00110000
#define MC1000_SG4_BRANCO   0b01000000
#define MC1000_SG4_CIANO    0b01010000
#define MC1000_SG4_MAGENTA  0b01100000
#define MC1000_SG4_LARANJA  0b01110000

#define MC1000_SG6_MASCARA_COR   0b11000000
#define MC1000_SG6_MASCARA_BLOCO 0b00111111

#define MC1000_SG6_VERDE    0b00000000
#define MC1000_SG6_AMARELO  0b01000000
#define MC1000_SG6_AZUL     0b10000000
#define MC1000_SG6_VERMELHO 0b11000000

#define MC1000_SG6_BRANCO   0b00000000
#define MC1000_SG6_CIANO    0b01000000
#define MC1000_SG6_MAGENTA  0b10000000
#define MC1000_SG6_LARANJA  0b11000000

#define MC1000_CG_VERDE    0b00
#define MC1000_CG_AMARELO  0b01
#define MC1000_CG_AZUL     0b10
#define MC1000_CG_VERMELHO 0b11

#define MC1000_CG_BYTE_VERDE    0b00000000
#define MC1000_CG_BYTE_AMARELO  0b01010101
#define MC1000_CG_BYTE_AZUL     0b10101010
#define MC1000_CG_BYTE_VERMELHO 0b11111111

#define MC1000_CG_BRANCO   0b00
#define MC1000_CG_CIANO    0b01
#define MC1000_CG_MAGENTA  0b10
#define MC1000_CG_LARANJA  0b11

#define MC1000_CG_BYTE_BRANCO  0b00000000
#define MC1000_CG_BYTE_CIANO   0b01010101
#define MC1000_CG_BYTE_MAGENTA 0b10101010
#define MC1000_CG_BYTE_LARANJA 0b11111111

#define MC1000_RG_PRETO  0b0
#define MC1000_RG_BRANCO 0b1

#define MC1000_RG_BYTE_PRETO  0b00000000
#define MC1000_RG_BYTE_BRANCO 0b11111111

#define MC1000_RG_VERDE_ESCURO 0b0
#define MC1000_RG_VERDE        0b1

#define MC1000_RG_BYTE_VERDE_ESCURO 0b00000000
#define MC1000_RG_BYTE_VERDE        0b11111111

// ----------------------------------------------------------------------------

// Declarações de rotinas em linguagem de máquina.

extern void preencheVRAM(unsigned int modoDeVideo_e_byteDeCor) __naked __z88dk_fastcall;
extern void preencheVRAMEmModbuf(unsigned char byteDeCor) __naked __z88dk_fastcall;

extern void printHexInt(unsigned int i) __z88dk_fastcall __naked;
extern void printHexChar(unsigned char c) __z88dk_fastcall __naked;
extern void print(char* s) __z88dk_fastcall __naked;

// ----------------------------------------------------------------------------


// Pseudoinstrução para inserir um breakpoint para o monitor (DEBUG) do MC1000.
#define MC1000_DEBUG() __asm rst 0x30 __endasm

// Pseudoinstrução para ativar a VRAM.
#define MC1000_ATIVA_VRAM( x ) MC1000_COL32 = x & MC1000_COL32_MASCARA_VRAM_INV

// Pseudoinstrução para desativar a VRAM.
#define MC1000_INATIVA_VRAM( x ) MC1000_COL32 = x | MC1000_COL32_VRAM_INATIVA

// ----------------------------------------------------------------------------

// Pseudoinstructions interacting with the game.

#define MACHINE_INIT() \
	MC1000_MODBUF = MC1000_COL32_CG6 | MC1000_COL32_CSS0 | MC1000_COL32_VRAM_INATIVA; \
	preencheVRAMEmModbuf(BYTE_FUNDO);
