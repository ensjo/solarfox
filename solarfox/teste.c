/*
 * teste.c
 *
 *  Created on: 29 de nov de 2017
 *      Author: 44868634291
 */

void giraEternamente() {
	signed char dx = 0, dy = -1;
	unsigned char xxDiv12, xxMod12;
	unsigned char yyDiv16, yyMod16;

	unsigned char continuar = 1;

	while (continuar) {

		MC1000_MODBUF = MC1000_COL32_CG6 | MC1000_COL32_CSS1 | MC1000_COL32_VRAM_INATIVA;

		limpaBuffer();
		insereSolarFox(0);

		solarFox.x += dx;
		xxDiv12 = (solarFox.x - 22) / 12;
		xxMod12 = (solarFox.x - 22) % 12;

		solarFox.y += dy;
		yyDiv16 = (solarFox.y - 32) / 16;
		yyMod16 = (solarFox.y - 32) % 16;

		if (solarFox.direcao == SF_DIRECAO_ESQUERDA || solarFox.direcao == SF_DIRECAO_DIREITA) {
			if (xxMod12 < 7 && rackData.matrix[yyDiv16][xxDiv12]) {
				insereFigura(5 + 3 * xxDiv12, 32 + 16 * yyDiv16, solarCellSprite, 0, BYTE_FUNDO, BRUTO);
			}
			if (xxDiv12 < 7 && xxMod12 > 5 && rackData.matrix[yyDiv16][xxDiv12 + 1]) {
				insereFigura(5 + 3 * (xxDiv12 + 1), 32 + 16 * yyDiv16, solarCellSprite, 0, BYTE_FUNDO, BRUTO);
			}
		} else {
			if (yyMod16 < 10 && rackData.matrix[yyDiv16][xxDiv12]) {
				insereFigura(5 + 3 * xxDiv12, 32 + 16 * yyDiv16, solarCellSprite, 0, BYTE_FUNDO, BRUTO);
			}
			if (yyDiv16 < 8 && yyMod16 > 5 && rackData.matrix[yyDiv16 + 1][xxDiv12]) {
				insereFigura(5 + 3 * xxDiv12, 32 + 16 * (yyDiv16 + 1), solarCellSprite, 0, BYTE_FUNDO, BRUTO);
			}
		}

		switch (solarFox.direcao) {
		case SF_DIRECAO_CIMA:
			if (solarFox.y <= 32) {
				solarFox.y = 32;
				solarFox.direcao = SF_DIRECAO_ESQUERDA;
				dx = -1;
				dy = 0;
			}
			break;
		case SF_DIRECAO_ESQUERDA:
			if (solarFox.x <= 58) {
				solarFox.x = 58;
				solarFox.direcao = SF_DIRECAO_BAIXO;
				dx = 0;
				dy = +1;
			}
			break;
		case SF_DIRECAO_BAIXO:
			if (solarFox.y >= 160) {
				solarFox.y = 160;
				solarFox.direcao = SF_DIRECAO_DIREITA;
				dx = +1;
				dy = 0;
			}
			break;
		case SF_DIRECAO_DIREITA:
			if (solarFox.x >= 70) {
				solarFox.x = 70;
				solarFox.direcao = SF_DIRECAO_CIMA;
				dx = 0;
				dy = -1;
			}
			break;
		default:
			// ERRO!
			MC1000_COL32 = MC1000_MODBUF = MC1000_COL32_CG6 | MC1000_COL32_CSS1 | MC1000_COL32_VRAM_INATIVA;
			return;
		}

		if (solarFox.x == 70 && solarFox.y == 96) {
			if (++rackData.rackNumber >= RACK_PATTERN_QTY) {
				continuar = 0;
			} else {
				rackData.rackNumberOnScreen++;
				drawRackNumberOnScreen();
				inicializaRack();
				insereRack();
			}
		}

		insereSolarFox(1);

		MC1000_MODBUF = MC1000_COL32_CG6 | MC1000_COL32_CSS0 | MC1000_COL32_VRAM_INATIVA;
		desenhaBuffer();

	}
}

void teste() {
	inicializaRack();

	limpaBuffer();
	insereRack();
	insereSolarFox(1);
	desenhaBuffer();

	drawSprite(0, 192-7, rackLabelSprite, MC1000_CG_BYTE_AMARELO, BYTE_FUNDO, COR_FRENTE_E_TRANSPARENCIA);
	drawRackNumberOnScreen();

	giraEternamente();
}
