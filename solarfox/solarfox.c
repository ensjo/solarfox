#define TESTE (1)

#include "../mc1000/mc1000.h"
#include "../bufferdevideo/bufferdevideo.h"
#include "racks.c"
#include "sprites.c"

#define BYTE_FUNDO MC1000_CG_BYTE_AZUL

#define SF_DIRECAO_CIMA 0
#define SF_DIRECAO_BAIXO 1
#define SF_DIRECAO_ESQUERDA 2
#define SF_DIRECAO_DIREITA 3

struct {
	unsigned char x;
	unsigned char y;
	unsigned char direcao;
} solarFox;

unsigned char userHasQuitGame = 0;

void initGame() {
	INIT_MACHINE();
	
	rackData.rackIsChallenge = 0;
	rackData.rackNumberOnScreen = 1;
	rackData.rackNumber = 0;
	rackData.challengeRackNumber = 0;
}

void drawNumber(unsigned char x, unsigned char y, int number, unsigned char digits, unsigned char trailingZeroes, unsigned char fgColorByte, unsigned char bgColorByte) {
	const unsigned int powersOf10[] = { 1, 10, 100, 1000, 10000 };
	unsigned int powerOf10;
	unsigned char digit;

	while (digits > 0) {
		digit = 0;
		powerOf10 = powersOf10[digits - 1];
		while (number >= powerOf10) {
			number -= powerOf10;
			digit++;
		}
		if (digit || digits == 1) {
			trailingZeroes = 1;
		}
		drawSprite(x, y, digitSprite[digit], digit || trailingZeroes ? fgColorByte : bgColorByte, bgColorByte, COR_FRENTE_E_TRANSPARENCIA);

		digits--;
		x += 2;
	}
}

void drawRackNumberOnScreen() {
	drawNumber(7, 192-7, (int) rackData.rackNumberOnScreen, 2, 0, MC1000_CG_BYTE_AMARELO, BYTE_FUNDO);
}

void inicializaRack() {
	unsigned char row, col, mask;
	rackPattern_t* rackPatternPtr;
	unsigned char cellLayers;
	
	solarFox.x = 70;
	solarFox.y = 96;
	solarFox.direcao = SF_DIRECAO_CIMA;
	
	if (rackData.rackIsChallenge) {
		cellLayers = 1;
		rackPatternPtr = &(challengeRackPattern[rackData.challengeRackNumber]);
	} else {
		cellLayers = rackData.rackNumber < 6 ? 1 : 2;
		rackPatternPtr = &(rackPattern[rackData.rackNumber]);
	}
	rackData.remainingCells = 0;

	for (row = 0; row < 9; row++) {
		mask = 0b10000000;
		for (col = 0; col < 8; col++) {
			if (rackPatternPtr->matrix[row] & mask) {
				rackData.matrix[row][col] = cellLayers;
				rackData.remainingCells += cellLayers;
			} else {
				rackData.matrix[row][col] = 0;
			}
			mask >>= 1;
		}
	}

	rackData.timer = 255;
}

void insereSolarFox(unsigned char desenhaOuApaga) {
	unsigned char* figura;

	switch (solarFox.direcao) {
	case SF_DIRECAO_ESQUERDA:
		switch (solarFox.x & 0b00000011) {
		case 0:
			figura = solarFoxL0Sprite;
			break;
		case 1:
			figura = solarFoxL1Sprite;
			break;
		case 2:
			figura = solarFoxL2Sprite;
			break;
		case 3:
			figura = solarFoxL3Sprite;
			break;
		default:
			// ERRO!
			return;
		}
		break;
	case SF_DIRECAO_DIREITA:
		switch (solarFox.x & 0b00000011) {
		case 0:
			figura = solarFoxR0Sprite;
			break;
		case 1:
			figura = solarFoxR1Sprite;
			break;
		case 2:
			figura = solarFoxR2Sprite;
			break;
		case 3:
			figura = solarFoxR3Sprite;
			break;
		default:
			// ERRO!
			return;
		}
		break;
	case SF_DIRECAO_CIMA:
		figura = solarFoxUSprite;
		break;
	case SF_DIRECAO_BAIXO:
		figura = solarFoxDSprite;
		break;
	default:
		// ERRO!
		return;
	}

	insereFigura(
		solarFox.x >> 2,
		solarFox.y,
		figura,
		MC1000_CG_BYTE_AMARELO,
		BYTE_FUNDO,
		desenhaOuApaga ? COR_FRENTE_E_TRANSPARENCIA : COR_FUNDO
	);
}

void insereRack() {
	unsigned char row, col;
	
	for (row = 0; row < 9; row++) {
		for (col = 0; col < 8; col++) {
			insereFigura(
				5 + col * 3,
				32 + row * 16,
				(unsigned char*) solarCellSprite,
				0,
				BYTE_FUNDO,
				rackData.matrix[row][col] ? BRUTO : COR_FUNDO
			);
		}
	}
}

#if TESTE
#include "teste.c"
#endif /* TESTE */

void main() {
	m_before_initGame();
	initGame();

#if TESTE

	teste();

#else

	while (!userHasQuitGame) {

		demo();

		if (userHasQuitGame) break;

		play();

	}

	quitGame();
	m_after_quitGame();
#endif
}

// coordenadas em CG6 (128x192)
// Centro celula solar 0,0 = 22,32 (pixel mais central do quadrante superior esquerdo da figura)
// Centro celula solar 1,0 = 34,32 +(12,0)
// Centro celula solar 0,1 = 22,48 +(0,16)

// 8x9 celulas solares (0 A 71)
