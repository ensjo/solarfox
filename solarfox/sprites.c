/*
 * sprites.c
 *
 *  Created on: 27 de nov de 2017
 *      Author: Emerson José Silveira da Costa.
 */

// SOLAR FOX ship, facing up:
//          |..XXXX..|
//          |..XXXX..|
//          |..XXXX..|
//          |..XXXX..|
//          |..XXXX..|
//          |..XXXX..|
//          <..XX@@..>
//          |XXXXXXXX|
// |......XX|XXXXXXXX|XX......|
// |....XXXX|XXXXXXXX|XXXX....|
// |....XXXX|..XXXX..|XXXX....|
// |....XX..|        |..XX....|

const unsigned char solarFoxUSprite[] = {
	(unsigned char) -1, // dx
	(unsigned char) +2, // dy
	4, // qty
	0b00000011,
	0b00001111,
	0b00001111,
	0b00001100,
	(unsigned char) +0, // dx
	(unsigned char) -6, // dy
	11, // qty
	0b00111100,
	0b00111100,
	0b00111100,
	0b00111100,
	0b00111100,
	0b00111100,
	0b00111100,
	0b11111111,
	0b11111111,
	0b11111111,
	0b00111100,
	(unsigned char) +1, // dx
	(unsigned char) +2, // dy
	4, // qty
	0b11000000,
	0b11110000,
	0b11110000,
	0b00110000,
	(unsigned char) -128 // End of sprite.
};

// SOLAR FOX ship, facing down:
// |....XX..|        |..XX....|
// |....XXXX|..XXXX..|XXXX....|
// |....XXXX|XXXXXXXX|XXXX....|
// |......XX|XXXXXXXX|XX......|
//          |XXXXXXXX|
//          |..XXXX..|
//          <..XX@@..>
//          |..XXXX..|
//          |..XXXX..|
//          |..XXXX..|
//          |..XXXX..|
//          |..XXXX..|

const unsigned char solarFoxDSprite[] = {
	(unsigned char) -1, // dx
	(unsigned char) -6, // dy
	4, // qty
	0b00001100,
	0b00001111,
	0b00001111,
	0b00000011,
	(unsigned char) +0, // dx
	(unsigned char) -5, // dy
	11, // qty
	0b00111100,
	0b11111111,
	0b11111111,
	0b11111111,
	0b00111100,
	0b00111100,
	0b00111100,
	0b00111100,
	0b00111100,
	0b00111100,
	0b00111100,
	(unsigned char) +1, // dx
	(unsigned char) -6, // dy
	4, // qty
	0b00110000,
	0b11110000,
	0b11110000,
	0b11000000,
	(unsigned char) -128 // End of sprite.
};

// SOLAR FOX ship, facing left (1/4):
//          |....XXXX|
//          |..XXXXXX|
//          |XXXXXX..|
// |XXXXXXXX|XXXXXXXX|
// |XXXXXXXX<@@XXXXXX>
//          |XXXXXX..|
//          |..XXXXXX|
//          |....XXXX|

const unsigned char solarFoxL0Sprite[] = {
	(unsigned char) -1, // dx
	(unsigned char) -1, // dy
	2, // qty
	0b11111111,
	0b11111111,
	(unsigned char) 0, // dx
	(unsigned char) -4, // dy
	8, // qty
	0b00001111,
	0b00111111,
	0b11111100,
	0b11111111,
	0b11111111,
	0b11111100,
	0b00111111,
	0b00001111,
	(unsigned char) -128 // End of sprite.
};

// SOLAR FOX ship, facing left (2/4):
//          |......XX|XX......|
//          |....XXXX|XX......|
//          |..XXXXXX|........|
// |..XXXXXX|XXXXXXXX|XX......|
// |..XXXXXX<XX@@XXXX>XX......|
//          |..XXXXXX|........|
//          |....XXXX|XX......|
//          |......XX|XX......|

const unsigned char solarFoxL1Sprite[] = {
	(unsigned char) -1, // dx
	(unsigned char) -1, // dy
	2, // qty
	0b00111111,
	0b00111111,
	(unsigned char) +0, // dx
	(unsigned char) -4, // dy
	8, // qty
	0b00000011,
	0b00001111,
	0b00111111,
	0b11111111,
	0b11111111,
	0b00111111,
	0b00001111,
	0b00000011,
	(unsigned char) +1, // dx
	(unsigned char) -4, // dy
	8, // qty
	0b11000000,
	0b11000000,
	0b00000000,
	0b11000000,
	0b11000000,
	0b00000000,
	0b11000000,
	0b11000000,
	(unsigned char) -128 // End of sprite.
};

// SOLAR FOX ship, facing left (3/4):
//                   |XXXX....|
//          |......XX|XXXX....|
//          |....XXXX|XX......|
// |....XXXX|XXXXXXXX|XXXX....|
// |....XXXX<XXXX@@XX>XXXX....|
//          |....XXXX|XX......|
//          |......XX|XXXX....|
//                   |XXXX....|

const unsigned char solarFoxL2Sprite[] = {
	(unsigned char) -1, // dx
	(unsigned char) -1, // dy
	2, // qty
	0b00001111,
	0b00001111,
	(unsigned char) +0, // dx
	(unsigned char) -3, // dy
	6, // qty
	0b00000011,
	0b00001111,
	0b11111111,
	0b11111111,
	0b00001111,
	0b00000011,
	(unsigned char) +1, // dx
	(unsigned char) -4, // dy
	8, // qty
	0b11110000,
	0b11110000,
	0b11000000,
	0b11110000,
	0b11110000,
	0b11000000,
	0b11110000,
	0b11110000,
	(unsigned char) -128 // End of sprite.
};

// SOLAR FOX ship, facing left (4/4):
//                   |..XXXX..|
//                   |XXXXXX..|
//          |......XX|XXXX....|
// |......XX|XXXXXXXX|XXXXXX..|
// |......XX<XXXXXX@@>XXXXXX..|
//          |......XX|XXXX....|
//                   |XXXXXX..|
//                   |..XXXX..|

const unsigned char solarFoxL3Sprite[] = {
	(unsigned char) -1, // dx
	(unsigned char) -1, // dy
	2, // qty
	0b00000011,
	0b00000011,
	(unsigned char) +0, // dx
	(unsigned char) -3, // dy
	4, // qty
	0b00000011,
	0b11111111,
	0b11111111,
	0b00000011,
	(unsigned char) +1, // dx
	(unsigned char) -4, // dy
	8, // qty
	0b00111100,
	0b11111100,
	0b11110000,
	0b11111100,
	0b11111100,
	0b11110000,
	0b11111100,
	0b00111100,
	(unsigned char) -128 // End of sprite.
};

// SOLAR FOX ship, heading right (1/4):
// |XXXX....|
// |XXXXXX..|
// |..XXXXXX|
// |XXXXXXXX|XXXXXXXX|
// |XXXXXXXX<@@XXXXXX>
// |..XXXXXX|
// |XXXXXX..|
// |XXXX....|

const unsigned char solarFoxR0Sprite[] = {
	(unsigned char) -1, // dx
	(unsigned char) -4, // dy
	8, // qty
	0b11110000,
	0b11111100,
	0b00111111,
	0b11111111,
	0b11111111,
	0b00111111,
	0b11111100,
	0b11110000,
	(unsigned char) +0, // dx
	(unsigned char) -1, // dy
	2, // qty
	0b11111111,
	0b11111111,
	(unsigned char) -128 // End of sprite.
};

// SOLAR FOX ship, heading right (2/4):
// |..XXXX..|
// |..XXXXXX|
// |....XXXX|XX......|
// |..XXXXXX|XXXXXXXX|XX......|
// |..XXXXXX<XX@@XXXX>XX......|
// |....XXXX|XX......|
// |..XXXXXX|
// |..XXXX..|

const unsigned char solarFoxR1Sprite[] = {
	(unsigned char) -1, // dx
	(unsigned char) -4, // dy
	8, // qty
	0b00111100,
	0b00111111,
	0b00001111,
	0b00111111,
	0b00111111,
	0b00001111,
	0b00111111,
	0b00111100,
	(unsigned char) +0, // dx
	(unsigned char) -2, // dy
	4, // qty
	0b11000000,
	0b11111111,
	0b11111111,
	0b11000000,
	(unsigned char) +1, // dx
	(unsigned char) -1, // dy
	2, // qty
	0b11000000,
	0b11000000,
	(unsigned char) -128 // End of sprite.
};

// SOLAR FOX ship, heading right (3/4):
// |....XXXX|
// |....XXXX|XX......|
// |......XX|XXXX....|
// |....XXXX|XXXXXXXX|XXXX....|
// |....XXXX<XXXX@@XX>XXXX....|
// |......XX|XXXX....|
// |....XXXX|XX......|
// |....XXXX|

const unsigned char solarFoxR2Sprite[] = {
	(unsigned char) -1, // dx
	(unsigned char) -4, // dy
	8, // qty
	0b00001111,
	0b00001111,
	0b00000011,
	0b00001111,
	0b00001111,
	0b00000011,
	0b00001111,
	0b00001111,
	(unsigned char) +0, // dx
	(unsigned char) -3, // dy
	6, // qty
	0b11000000,
	0b11110000,
	0b11111111,
	0b11111111,
	0b11110000,
	0b11000000,
	(unsigned char) +1, // dx
	(unsigned char) -1, // dy
	2, // qty
	0b11110000,
	0b11110000,
	(unsigned char) -128 // End of sprite.
};

// SOLAR FOX ship, heading right (4/4):
// |......XX|XX......|
// |......XX|XXXX....|
// |........|XXXXXX..|
// |......XX|XXXXXXXX|XXXXXX..|
// |......XX<XXXXXX@@>XXXXXX..|
// |........|XXXXXX..|
// |......XX|XXXX....|
// |......XX|XX......|


const unsigned char solarFoxR3Sprite[] = {
	(unsigned char) -1, // dx
	(unsigned char) -4, // dy
	8, // qty
	0b00000011,
	0b00000011,
	0b00000000,
	0b00000011,
	0b00000011,
	0b00000000,
	0b00000011,
	0b00000011,
	(unsigned char) +0, // dx
	(unsigned char) -4, // dy
	8, // qty
	0b11000000,
	0b11110000,
	0b11111100,
	0b11111111,
	0b11111111,
	0b11111100,
	0b11110000,
	0b11000000,
	(unsigned char) +1, // dx
	(unsigned char) -1, // dy
	2, // qty
	0b11111100,
	0b11111100,
	(unsigned char) -128 // End of sprite.
};

// SENTINEL, heading up (1/4):
// |..XX....|....XX..|
// |XXXX..XX|XX..XXXX|
// |XX....XX|XX....XX|
// |XXXXXXXX|XXXXXXXX|
// |XXXXXXXX<@@XXXXXX>

const unsigned char sentinelU0Sprite[] = {
	(unsigned char) -1, // dx
	(unsigned char) -4, // dy
	5, // qty
	0b00110000,
	0b11110011,
	0b11000011,
	0b11111111,
	0b11111111,
	(unsigned char) +0, // dx
	(unsigned char) -4, // dy
	5, // qty
	0b00001100,
	0b11001111,
	0b11000011,
	0b11111111,
	0b11111111,
	(unsigned char) -128 // End of sprite.
};

// SENTINEL, heading up (2/4):
// |....XX..|......XX|
// |..XXXX..|XXXX..XX|XX......|
// |..XX....|XXXX....|XX......|
// |..XXXXXX|XXXXXXXX|XX......|
// |..XXXXXX<XX@@XXXX>XX......|

const unsigned char sentinelU1Sprite[] = {
	(unsigned char) -1, // dx
	(unsigned char) -4, // dy
	5, // qty
	0b00001100,
	0b00111100,
	0b00110000,
	0b00111111,
	0b00111111,
	(unsigned char) +0, // dx
	(unsigned char) -4, // dy
	5, // qty
	0b00000011,
	0b11110011,
	0b11110000,
	0b11111111,
	0b11111111,
	(unsigned char) +1, // dx
	(unsigned char) -3, // dy
	4, // qty
	0b11000000,
	0b11000000,
	0b11000000,
	0b11000000,
	(unsigned char) -128 // End of sprite.
};

// SENTINEL, heading up (3/4):
// |......XX|        |XX......|
// |....XXXX|..XXXX..|XXXX....|
// |....XX..|..XXXX..|..XX....|
// |....XXXX|XXXXXXXX|XXXX....|
// |....XXXX<XXXX@@XX>XXXX....|

const unsigned char sentinelU2Sprite[] = {
	(unsigned char) -1, // dx
	(unsigned char) -4, // dy
	5, // qty
	0b00000011,
	0b00001111,
	0b00001100,
	0b00001111,
	0b00001111,
	(unsigned char) +0, // dx
	(unsigned char) -3, // dy
	4, // qty
	0b00111100,
	0b00111100,
	0b11111111,
	0b11111111,
	(unsigned char) +1, // dx
	(unsigned char) -4, // dy
	5, // qty
	0b11000000,
	0b11110000,
	0b00110000,
	0b11110000,
	0b11110000,
	(unsigned char) -128 // End of sprite.
};

// SENTINEL, heading up (4/4):
//          |XX......|..XX....|
// |......XX|XX..XXXX|..XXXX..|
// |......XX|....XXXX|....XX..|
// |......XX|XXXXXXXX|XXXXXX..|
// |......XX<XXXXXX@@>XXXXXX..|

const unsigned char sentinelU3Sprite[] = {
	(unsigned char) -1, // dx
	(unsigned char) -3, // dy
	4, // qty
	0b00000011,
	0b00000011,
	0b00000011,
	0b00000011,
	(unsigned char) +0, // dx
	(unsigned char) -4, // dy
	5, // qty
	0b11000000,
	0b11001111,
	0b00001111,
	0b11111111,
	0b11111111,
	(unsigned char) +1, // dx
	(unsigned char) -4, // dy
	5, // qty
	0b00110000,
	0b00111100,
	0b00001100,
	0b11111100,
	0b11111100,
	(unsigned char) -128 // End of sprite.
};

// SENTINEL, heading down (1/4):
// |XXXXXXXX|@@XXXXXX|
// |XXXXXXXX|XXXXXXXX|
// |XX....XX|XX....XX|
// |XXXX..XX|XX..XXXX|
// |..XX....|....XX..|

const unsigned char sentinelD0Sprite[] = {
	(unsigned char) -1, // dx
	(unsigned char) +0, // dy
	5, // qty
	0b11111111,
	0b11111111,
	0b11000011,
	0b11110011,
	0b00110000,
	(unsigned char) +0, // dx
	(unsigned char) +0, // dy
	5, // qty
	0b11111111,
	0b11111111,
	0b11000011,
	0b11001111,
	0b00001100,
	(unsigned char) -128 // End of sprite.
};

// SENTINEL, heading down (2/4):
// |..XXXXXX<XX@@XXXX>XX......|
// |..XXXXXX|XXXXXXXX|XX......|
// |..XX....|XXXX....|XX......|
// |..XXXX..|XXXX..XX|XX......|
// |....XX..|......XX|

const unsigned char sentinelD1Sprite[] = {
	(unsigned char) -1, // dx
	(unsigned char) +0, // dy
	5, // qty
	0b00111111,
	0b00111111,
	0b00110000,
	0b00111100,
	0b00001100,
	(unsigned char) +0, // dx
	(unsigned char) +0, // dy
	5, // qty
	0b11111111,
	0b11111111,
	0b11110000,
	0b11110011,
	0b00000011,
	(unsigned char) +1, // dx
	(unsigned char) +0, // dy
	4,
	0b11000000,
	0b11000000,
	0b11000000,
	0b11000000,
	(unsigned char) -128 // End of sprite.
};

// SENTINEL, heading down (3/4):
// |....XXXX<XXXX@@XX>XXXX....|
// |....XXXX|XXXXXXXX|XXXX....|
// |....XX..|..XXXX..|..XX....|
// |....XXXX|..XXXX..|XXXX....|
// |......XX|        |XX......|

const unsigned char sentinelD2Sprite[] = {
	(unsigned char) -1, // dx
	(unsigned char) +0, // dy
	5, // qty
	0b00001111,
	0b00001111,
	0b00001100,
	0b00001111,
	0b00000011,
	(unsigned char) +0, // dx
	(unsigned char) +0, // dy
	4, // qty
	0b11111111,
	0b11111111,
	0b00111100,
	0b00111100,
	(unsigned char) +1, // dx
	(unsigned char) +0, // dy
	5,
	0b11110000,
	0b11110000,
	0b00110000,
	0b11110000,
	0b11000000,
	(unsigned char) -128 // Fim do sprite.
};

// SENTINEL, heading down (4/4):
// |......XX<XXXXXX@@>XXXXXX..|
// |......XX|XXXXXXXX|XXXXXX..|
// |......XX|....XXXX|....XX..|
// |......XX|XX..XXXX|..XXXX..|
//          |XX......|..XX....|

const unsigned char sentinelD3Sprite[] = {
	(unsigned char) -1, // dx
	(unsigned char) +0, // dy
	4,
	0b00000011,
	0b00000011,
	0b00000011,
	0b00000011,
	(unsigned char) +0, // dx
	(unsigned char) +0, // dy
	5,
	0b11111111,
	0b11111111,
	0b00001111,
	0b11001111,
	0b11000000,
	(unsigned char) +1, // dx
	(unsigned char) +0, // dy
	5,
	0b11111100,
	0b11111100,
	0b00001100,
	0b00111100,
	0b00110000,
	(unsigned char) -128 // Fim do sprite.
};

// FIREBALL:
//          |..XXXX..|
// |......XX|XXXXXXXX|XX......|
// |......XX|XX....XX|XX......|
// |......XX<XX..::XX>XX......|
// |......XX|XXXXXXXX|XX......|
//          |..XXXX..|

const unsigned char fireBallSprite[] = {
	(unsigned char) -1, // dx
	(unsigned char) -2, // dy
	4, // qty
	0b00000011,
	0b00000011,
	0b00000011,
	0b00000011,
	(unsigned char) +0, // dx
	(unsigned char) -3, // dy
	6, // qty
	0b00111100,
	0b11111111,
	0b11000011,
	0b11000011,
	0b11111111,
	0b00111100,
	(unsigned char) +1, // dx
	(unsigned char) -2, // dy
	4, // qty
	0b11000000,
	0b11000000,
	0b11000000,
	0b11000000,
	(unsigned char) -128 // end of sprite
};

// SOLAR CELL:
// |XXXXXXXX|
// |XXXXXXXX|
// |XXXXXXXX|
// <XXXX@@XX>
// |XXXXXXXX|
// |XXXXXXXX|

const unsigned char solarCellSprite[] = {
	(unsigned char) +0, // dx
	(unsigned char) -3, // dy
	6, // qty
	0b01010101, // Yellow stripe.
	0b00000000, // Green stripe.
	0b11111111, // Red stripe.
	0b11111111, // Red stripe.
	0b00000000, // Green stripe.
	0b01010101, // Yellow stripe.
	(unsigned char) -128 // End of sprite.
};

// LIFE TOKEN.
// <@@XXXXXX>
// |XXXXXXXX|
// |XXXXXXXX|
// |XXXXXXXX|
// |XXXXXXXX|
// |XXXXXXXX|
// |XXXXXXXX|

const unsigned char lifeTokenSprite[] = {
	(unsigned char) +0, // dx
	(unsigned char) +0, // dy
	7, // qty
	0b11111111,
	0b11111111,
	0b11111111,
	0b11111111,
	0b11111111,
	0b11111111,
	0b11111111,
	(unsigned char) -128, // End of sprite.
};

// DIGIT ZERO:
// <::XXXXXX>XXXX....|
// |XXXX....|..XXXX..|
// |XXXX....|..XXXX..|
// |XXXX....|..XXXX..|
// |XXXX....|..XXXX..|
// |XXXX....|..XXXX..|
// |..XXXXXX|XXXX....|

const unsigned char digit0Sprite[] = {
	(unsigned char) +0, // dx
	(unsigned char) +0, // dy
	7, // qty
	0b00111111,
	0b11110000,
	0b11110000,
	0b11110000,
	0b11110000,
	0b11110000,
	0b00111111,
	(unsigned char) +1, // dx
	(unsigned char) +0, // dy
	7,
	0b11110000,
	0b00111100,
	0b00111100,
	0b00111100,
	0b00111100,
	0b00111100,
	0b11110000,
	(unsigned char) -128 // End of sprite.
};

// DIGIT ONE:
// <::..XXXX>........|
// |..XXXXXX|........|
// |....XXXX|........|
// |....XXXX|........|
// |....XXXX|........|
// |....XXXX|........|
// |XXXXXXXX|XXXX....|

const unsigned char digit1Sprite[] = {
	(unsigned char) +0, // dx
	(unsigned char) +0, // dy
	7, // qty
	0b00001111,
	0b00111111,
	0b00001111,
	0b00001111,
	0b00001111,
	0b00001111,
	0b11111111,
	(unsigned char) +1, // dx
	(unsigned char) +0, // dy
	7,
	0b00000000,
	0b00000000,
	0b00000000,
	0b00000000,
	0b00000000,
	0b00000000,
	0b11110000,
	(unsigned char) -128 // End of sprite.
};

// DIGIT TWO:
// <::XXXXXX>XXXX....|
// |XXXX....|..XXXX..|
// |........|..XXXX..|
// |....XXXX|XXXX....|
// |..XXXXXX|XX......|
// |XXXXXX..|........|
// |XXXXXXXX|XXXXXX..|

const unsigned char digit2Sprite[] = {
	(unsigned char) +0, // dx
	(unsigned char) +0, // dy
	7,
	0b00111111,
	0b11110000,
	0b00000000,
	0b00001111,
	0b00111111,
	0b11111100,
	0b11111111,
	(unsigned char) +1, // dx
	(unsigned char) +0, // dy
	7,
	0b11110000,
	0b00111100,
	0b00111100,
	0b11110000,
	0b11000000,
	0b00000000,
	0b11111100,
	(unsigned char) -128 // End of sprite.
};

// DIGIT THREE:
// <::XXXXXX>XXXXXX..|
// |........|XXXX....|
// |......XX|XX......|
// |....XXXX|XXXX....|
// |........|..XXXX..|
// |XXXX....|..XXXX..|
// |..XXXXXX|XXXX....|

const unsigned char digit3Sprite[] = {
	(unsigned char) +0, // dx
	(unsigned char) +0, // dy
	7, // qty
	0b00111111,
	0b00000000,
	0b00000011,
	0b00001111,
	0b00000000,
	0b11110000,
	0b00111111,
	(unsigned char) +1, // dx
	(unsigned char) +0, // dy
	7, // qty
	0b11111100,
	0b11110000,
	0b11000000,
	0b11110000,
	0b00111100,
	0b00111100,
	0b11110000,
	(unsigned char) -128 // End of sprite.
};

// DIGIT FOUR:
// <::....XX>XXXX....|
// |....XXXX|XXXX....|
// |..XXXX..|XXXX....|
// |XXXX....|XXXX....|
// |XXXXXXXX|XXXX....|
// |........|XXXX....|
// |........|XXXX....|

const unsigned char digit4Sprite[] = {
	(unsigned char) +0, // dx
	(unsigned char) +0, // dy
	7,
	0b00000011,
	0b00001111,
	0b00111100,
	0b11110000,
	0b11111111,
	0b00000000,
	0b00000000,
	(unsigned char) +1, // dx
	(unsigned char) +0, // dy
	7,
	0b11110000,
	0b11110000,
	0b11110000,
	0b11110000,
	0b11110000,
	0b11110000,
	0b11110000,
	(unsigned char) -128 // End of sprite.
};

// DIGIT FIVE:
// <@@XXXXXX|XXXXXX..|
// |XXXX....|........|
// |XXXXXXXX|XXXX....|
// |........|..XXXX..|
// |........|..XXXX..|
// |XXXX....|..XXXX..|
// |..XXXXXX|XXXX....|

const unsigned char digit5Sprite[] = {
	(unsigned char) +0, // dx
	(unsigned char) +0, // dy
	7, // qty
	0b11111111,
	0b11110000,
	0b11111111,
	0b00000000,
	0b00000000,
	0b11110000,
	0b00111111,
	(unsigned char) +1, // dx
	(unsigned char) +0, // dy
	7, // qty
	0b11111100,
	0b00000000,
	0b11110000,
	0b00111100,
	0b00111100,
	0b00111100,
	0b11110000,
	(unsigned char) -128 // End of sprite.
};

// DIGIT SIX:
// <::..XXXX>XXXX....|
// |..XXXX..|........|
// |XXXX....|........|
// |XXXXXXXX|XXXX....|
// |XXXX....|..XXXX..|
// |XXXX....|..XXXX..|
// |..XXXXXX|XXXX....|

const unsigned char digit6Sprite[] = {
	(unsigned char) +0, // dx
	(unsigned char) +0, // dy
	7, // qty
	0b00001111,
	0b00111100,
	0b11110000,
	0b11111111,
	0b11110000,
	0b11110000,
	0b00111111,
	(unsigned char) +1,
	(unsigned char) +0,
	7, // qty
	0b11110000,
	0b00000000,
	0b00000000,
	0b11110000,
	0b00111100,
	0b00111100,
	0b11110000,
	(unsigned char) -128 // End of sprite.
};

// DIGIT SEVEN:
// <@@XXXXXX>XXXX....|
// |XXXX....|..XXXX..|
// |........|XXXX....|
// |......XX|XX......|
// |....XXXX|........|
// |....XXXX|........|
// |....XXXX|........|

const unsigned char digit7Sprite[] = {
	(unsigned char) +0, // dx
	(unsigned char) +0, // dy
	7, // qty
	0b11111111,
	0b11110000,
	0b00000000,
	0b00000011,
	0b00001111,
	0b00001111,
	0b00001111,
	(unsigned char) +1, // dx
	(unsigned char) +0, // dy
	7, // qty
	0b11110000,
	0b00111100,
	0b11110000,
	0b11000000,
	0b00000000,
	0b00000000,
	0b00000000,
	(unsigned char) -128 // End of sprite.
};

// DIGIT EIGHT:
// <::XXXXXX>XXXX....|
// |XXXX....|..XXXX..|
// |XXXX....|..XXXX..|
// |..XXXXXX|XXXX....|
// |XXXX....|..XXXX..|
// |XXXX....|..XXXX..|
// |..XXXXXX|XXXX....|

const unsigned char digit8Sprite[] = {
	(unsigned char) +0, // dx
	(unsigned char) +0, // dy
	7, // qty
	0b00111111,
	0b11110000,
	0b11110000,
	0b00111111,
	0b11110000,
	0b11110000,
	0b00111111,
	(unsigned char) +1, // dx
	(unsigned char) +0, // dy
	7, // qty
	0b11110000,
	0b00111100,
	0b00111100,
	0b11110000,
	0b00111100,
	0b00111100,
	0b11110000,
	(unsigned char) -128 // End of sprite.
};

// DIGIT NINE:
// <::XXXXXX>XXXX....|
// |XXXX....|..XXXX..|
// |XXXX....|..XXXX..|
// |..XXXXXX|XXXXXX..|
// |........|..XXXX..|
// |........|XXXX....|
// |..XXXXXX|XX......|

const unsigned char digit9Sprite[] = {
	(unsigned char) +0, // dx
	(unsigned char) +0, // dy
	7, // qty
	0b00111111,
	0b11110000,
	0b11110000,
	0b00111111,
	0b00000000,
	0b00000000,
	0b00111111,
	(unsigned char) +1, // dx
	(unsigned char) +0, // dy
	7, // qty
	0b11110000,
	0b00111100,
	0b00111100,
	0b11111100,
	0b00111100,
	0b11110000,
	0b11000000,
	(unsigned char) -128 // End of sprite.
};

// "RACK" label:
// <....@@XX>XXXX....|..XXXXXX|......XX|XXXX....|XX......|XX......|
// |....XX..|....XX..|XX......|XX..XX..|....XX..|XX....XX|........|
// |....XX..|....XX..|XX......|XX..XX..|........|XX..XX..|........|
// |....XXXX|XXXX....|XXXXXXXX|XX..XX..|........|XXXX....|........|
// |....XX..|XX......|XX......|XX..XX..|........|XX..XX..|........|
// |....XX..|..XX....|XX......|XX..XX..|....XX..|XX....XX|........|
// |....XX..|....XX..|XX......|XX....XX|XXXX....|XX......|XX......|

const unsigned char rackLabelSprite[] = {
	(unsigned char) +0, // dx
	(unsigned char) +0, // dy
	7, // qty
	0b00001111,
	0b00001100,
	0b00001100,
	0b00001111,
	0b00001100,
	0b00001100,
	0b00001100,
	(unsigned char) +1, // dx
	(unsigned char) +0, // dy
	7, // qty
	0b11110000,
	0b00001100,
	0b00001100,
	0b11110000,
	0b11000000,
	0b00110000,
	0b00001100,
	(unsigned char) +2, // dx
	(unsigned char) +0, // dy
	7, // qty
	0b00111111,
	0b11000000,
	0b11000000,
	0b11111111,
	0b11000000,
	0b11000000,
	0b11000000,
	(unsigned char) +3, // dx
	(unsigned char) +0, // dy
	7, // qty
	0b00000011,
	0b11001100,
	0b11001100,
	0b11001100,
	0b11001100,
	0b11001100,
	0b11000011,
	(unsigned char) +4, // dx
	(unsigned char) +0, // dy
	7, // qty
	0b11110000,
	0b00001100,
	0b00000000,
	0b00000000,
	0b00000000,
	0b00001100,
	0b11110000,
	(unsigned char) +5, // dx
	(unsigned char) +0, // dy
	7, // qty
	0b11000000,
	0b11000011,
	0b11001100,
	0b11110000,
	0b11001100,
	0b11000011,
	0b11000000,
	(unsigned char) +6, // dx
	(unsigned char) +0, // dy
	7, // qty
	0b11000000,
	0b00000000,
	0b00000000,
	0b00000000,
	0b00000000,
	0b00000000,
	0b11000000,
	(unsigned char) -128 // End of sprite.
};

// "SKIP-A-RACK" counter:
// <....@@XX>XXXXXXXX|XXXXXXXX|XXXXXXXX|XXXXXXXX|XXXXXXXX|XXXXXXXX|XXXXXXXX|XXXXXXXX|XXXXXXXX|XXXXXXXX|XXXXXXXX|XXXX....|
// |....XXXX|......XX|..XX..XX|......XX|......XX|XXXXXXXX|......XX|XXXXXXXX|......XX|......XX|......XX|..XX..XX|XXXX....|
// |....XXXX|..XXXXXX|..XX..XX|XX..XXXX|..XX..XX|XXXXXXXX|..XX..XX|XXXXXXXX|..XX..XX|..XX..XX|..XXXXXX|..XX..XX|XXXX....|
// |....XXXX|......XX|....XXXX|XX..XXXX|......XX|......XX|......XX|......XX|....XXXX|......XX|..XXXXXX|....XXXX|XXXX....|
// |....XXXX|XXXX..XX|..XX..XX|XX..XXXX|..XXXXXX|XXXXXXXX|..XX..XX|XXXXXXXX|..XX..XX|..XX..XX|..XXXXXX|..XX..XX|XXXX....|
// |....XXXX|......XX|..XX..XX|......XX|..XXXXXX|XXXXXXXX|..XX..XX|XXXXXXXX|..XX..XX|..XX..XX|......XX|..XX..XX|XXXX....|
// |....XXXX|XXXXXXXX|XXXXXXXX|XXXXXXXX|XXXXXXXX|XXXXXXXX|XXXXXXXX|XXXXXXXX|XXXXXXXX|XXXXXXXX|XXXXXXXX|XXXXXXXX|XXXX....|

const unsigned char skipARackCounterSprite[] = {
	(unsigned char) +0, // dx
	(unsigned char) +0, // dy
	7, // qty
	0b00001111,
	0b00001111,
	0b00001111,
	0b00001111,
	0b00001111,
	0b00001111,
	0b00001111,
	(unsigned char) +1, // dx
	(unsigned char) +0, // dy
	7, // qty
	0b11111111,
	0b00000011,
	0b00111111,
	0b00000011,
	0b11110011,
	0b00000011,
	0b11111111,
	(unsigned char) +2, // dx
	(unsigned char) +0, // dy
	7, // qty
	0b11111111,
	0b00110011,
	0b00110011,
	0b00001111,
	0b00110011,
	0b00110011,
	0b11111111,
	(unsigned char) +3, // dx
	(unsigned char) +0, // dy
	7, // qty
	0b11111111,
	0b00000011,
	0b11001111,
	0b11001111,
	0b11001111,
	0b00000011,
	0b11111111,
	(unsigned char) +4, // dx
	(unsigned char) +0, // dy
	7, // qty
	0b11111111,
	0b00000011,
	0b00110011,
	0b00000011,
	0b00111111,
	0b00111111,
	0b11111111,
	(unsigned char) +5, // dx
	(unsigned char) +0, // dy
	7, // qty
	0b11111111,
	0b11111111,
	0b11111111,
	0b00000011,
	0b11111111,
	0b11111111,
	0b11111111,
	(unsigned char) +6, // dx
	(unsigned char) +0, // dy
	7, // qty
	0b11111111,
	0b00000011,
	0b00110011,
	0b00000011,
	0b00110011,
	0b00110011,
	0b11111111,
	(unsigned char) +7, // dx
	(unsigned char) +0, // dy
	7, // qty
	0b11111111,
	0b11111111,
	0b11111111,
	0b00000011,
	0b11111111,
	0b11111111,
	0b11111111,
	(unsigned char) +8, // dx
	(unsigned char) +0, // dy
	7, // qty
	0b11111111,
	0b00000011,
	0b00110011,
	0b00001111,
	0b00110011,
	0b00110011,
	0b11111111,
	(unsigned char) +9, // dx
	(unsigned char) +0, // dy
	7, // qty
	0b11111111,
	0b00000011,
	0b00110011,
	0b00000011,
	0b00110011,
	0b00110011,
	0b11111111,
	(unsigned char) +10, // dx
	(unsigned char) +0, // dy
	7, // qty
	0b11111111,
	0b00000011,
	0b00111111,
	0b00111111,
	0b00111111,
	0b00000011,
	0b11111111,
	(unsigned char) +11, // dx
	(unsigned char) +0, // dy
	7, // qty
	0b11111111,
	0b00110011,
	0b00110011,
	0b00001111,
	0b00110011,
	0b00110011,
	0b11111111,
	(unsigned char) +12, // dx
	(unsigned char) +0, // dy
	7, // qty
	0b11110000,
	0b11110000,
	0b11110000,
	0b11110000,
	0b11110000,
	0b11110000,
	0b11110000,
	(unsigned char) -128 // End of sprite.
};

// "CHALLENGE!" counter:
// <....@@XX>XXXXXXXX|XXXXXXXX|XXXXXXXX|XXXXXXXX|XXXXXXXX|XXXXXXXX|XXXXXXXX|XXXXXXXX|XXXXXXXX|XXXXXXXX|XXXXXXXX|XXXX....|
// |....XXXX|......XX|..XXXX..|XXXX....|XXXX..XX|XXXXXX..|XXXXXXXX|........|XX..XXXX|..XXXX..|....XX..|......XX|..XX....|
// |....XX..|XXXXXXXX|..XXXX..|XX..XXXX|..XX..XX|XXXXXX..|XXXXXXXX|..XXXXXX|XX....XX|..XX..XX|XXXXXX..|XXXXXXXX|..XX....|
// |....XX..|XXXXXXXX|........|XX......|..XX..XX|XXXXXX..|XXXXXXXX|......XX|XX..XX..|..XX..XX|XXXXXX..|....XXXX|..XX....|
// |....XX..|XXXXXXXX|..XXXX..|XX..XXXX|..XX..XX|XXXXXX..|XXXXXXXX|..XXXXXX|XX..XXXX|..XX..XX|XX..XX..|XXXXXXXX|XXXX....|
// |....XXXX|......XX|..XXXX..|XX..XXXX|..XX....|....XX..|......XX|........|XX..XXXX|..XXXX..|....XX..|......XX|..XX....|
// |....XXXX|XXXXXXXX|XXXXXXXX|XXXXXXXX|XXXXXXXX|XXXXXXXX|XXXXXXXX|XXXXXXXX|XXXXXXXX|XXXXXXXX|XXXXXXXX|XXXXXXXX|XXXX....|


const unsigned char challengeCounterSprite[] = {
	(unsigned char) +0, // dx
	(unsigned char) +0, // dy
	7, // qty
	0b00001111,
	0b00001111,
	0b00001100,
	0b00001100,
	0b00001100,
	0b00001111,
	0b00001111,
	(unsigned char) +1, // dx
	(unsigned char) +0, // dy
	7, // qty
	0b11111111,
	0b00000011,
	0b11111111,
	0b11111111,
	0b11111111,
	0b00000011,
	0b11111111,
	(unsigned char) +2, // dx
	(unsigned char) +0, // dy
	7, // qty
	0b11111111,
	0b00111100,
	0b00111100,
	0b00000000,
	0b00111100,
	0b00111100,
	0b11111111,
	(unsigned char) +3, // dx
	(unsigned char) +0, // dy
	7, // qty
	0b11111111,
	0b11110000,
	0b11001111,
	0b11000000,
	0b11001111,
	0b11001111,
	0b11111111,
	(unsigned char) +4, // dx
	(unsigned char) +0, // dy
	7, // qty
	0b11111111,
	0b11110011,
	0b00110011,
	0b00110011,
	0b00110011,
	0b00110000,
	0b11111111,
	(unsigned char) +5, // dx
	(unsigned char) +0, // dy
	7, // qty
	0b11111111,
	0b11111100,
	0b11111100,
	0b11111100,
	0b11111100,
	0b00111100,
	0b11111111,
	(unsigned char) +6, // dx
	(unsigned char) +0, // dy
	7, // qty
	0b11111111,
	0b11111111,
	0b11111111,
	0b11111111,
	0b11111111,
	0b00000011,
	0b11111111,
	(unsigned char) +7, // dx
	(unsigned char) +0, // dy
	7, // qty
	0b11111111,
	0b00000000,
	0b00111111,
	0b00000011,
	0b00111111,
	0b00000000,
	0b11111111,
	(unsigned char) +8, // dx
	(unsigned char) +0, // dy
	7, // qty
	0b11111111,
	0b11001111,
	0b11000011,
	0b11001100,
	0b11001111,
	0b11001111,
	0b11111111,
	(unsigned char) +9, // dx
	(unsigned char) +0, // dy
	7, // qty
	0b11111111,
	0b00111100,
	0b00110011,
	0b00110011,
	0b00110011,
	0b00111100,
	0b11111111,
	(unsigned char) +10, // dx
	(unsigned char) +0, // dy
	7, // qty
	0b11111111,
	0b00001100,
	0b11111100,
	0b11111100,
	0b11001100,
	0b00001100,
	0b11111111,
	(unsigned char) +11, // dx
	(unsigned char) +0, // dy
	7, // qty
	0b11111111,
	0b00000011,
	0b11111111,
	0b00001111,
	0b11111111,
	0b00000011,
	0b11111111,
	(unsigned char) +12, // dx
	(unsigned char) +0, // dy
	7, // qty
	0b11110000,
	0b00110000,
	0b00110000,
	0b00110000,
	0b11110000,
	0b00110000,
	0b11110000,
	(unsigned char) -128 // End of sprite.
};

// "CBS PRESENTS" label:
//                   |..XXXXXX|....XXXX<@@......>XXXXXXXX|
//                   |XX......|....XX..|XX......|XX......|
//                   |XX......|....XXXX|XX......|XXXX....|
//                   |XX......|....XX..|..XX....|....XXXX|
//                   |XX......|....XX..|..XX....|......XX|
//                   |..XXXXXX|....XXXX|XXXX....|XXXXXXXX|
// (4 lines)
// |XXXXXX..|XXXXXX..|XXXXXX..|XXXXXX..|XXXXXX..|XX....XX|..XXXXXX|..XXXXXX|
// |XX..XX..|XX..XX..|XX......|XX......|XX......|XXXX..XX|....XX..|..XX....|
// |XXXXXX..|XXXX....|XXXX....|XXXXXX..|XXXX....|XX..XXXX|....XX..|..XXXXXX|
// |XX......|XX..XX..|XX......|....XX..|XX......|XX....XX|....XX..|......XX|
// |XX......|XX..XX..|XXXXXX..|XXXX....|XXXXXX..|XX....XX|....XX..|..XXXXXX|

const unsigned char CBSPresentsLabelSprite[] = {
	(unsigned char) -2, // dx
	(unsigned char) +0, // dy
	6, // qty
	0b00111111,
	0b11000000,
	0b11000000,
	0b11000000,
	0b11000000,
	0b00111111,
	(unsigned char) -1, // dx
	(unsigned char) +0, // dy
	6, // qty
	0b00001111,
	0b00001100,
	0b00001111,
	0b00001100,
	0b00001100,
	0b00001111,
	(unsigned char) +0, // dx
	(unsigned char) +0, // dy
	6, // qty
	0b11000000,
	0b11000000,
	0b11000000,
	0b00110000,
	0b00110000,
	0b11110000,
	(unsigned char) +1, // dx
	(unsigned char) +0, // dy
	6, // qty
	0b11111111,
	0b11000000,
	0b11110000,
	0b00001111,
	0b00000011,
	0b11111111,
	(unsigned char) -4, // dx
	(unsigned char) +10, // dy
	5, // qty
	0b11111100,
	0b11001100,
	0b11111100,
	0b11000000,
	0b11000000,
	(unsigned char) -3, // dx
	(unsigned char) +10, // dy
	5, // qty
	0b11111100,
	0b11001100,
	0b11110000,
	0b11001100,
	0b11001100,
	(unsigned char) -2, // dx
	(unsigned char) +10, // dy
	5, // qty
	0b11111100,
	0b11000000,
	0b11110000,
	0b11000000,
	0b11111100,
	(unsigned char) -1, // dx
	(unsigned char) +10, // dy
	5, // qty
	0b11111100,
	0b11000000,
	0b11111100,
	0b00001100,
	0b11110000,
	(unsigned char) +0, // dx
	(unsigned char) +10, // dy
	5, // qty
	0b11111100,
	0b11000000,
	0b11110000,
	0b11000000,
	0b11111100,
	(unsigned char) +1, // dx
	(unsigned char) +10, // dy
	5, // qty
	0b11000011,
	0b11110011,
	0b11001111,
	0b11000011,
	0b11000011,
	(unsigned char) +2, // dx
	(unsigned char) +10, // dy
	5, // qty
	0b00111111,
	0b00001100,
	0b00001100,
	0b00001100,
	0b00001100,
	(unsigned char) +3, // dx
	(unsigned char) +10, // dy
	5, // qty
	0b00111111,
	0b00110000,
	0b00111111,
	0b00000011,
	0b00111111,
	(unsigned char) -128 // End of sprite.
};

// "SOLAR FOX" label:
// |XXXXXXXX|XXXXXX..|XXXXXXXX|XXXXXX..|XX......<::      >......XX|XX......|..XXXXXX|XXXXXX..|
// |XX......|........|XX......|....XX..|XX......|        |....XX..|..XX....|..XX....|....XX..|
// |XXXXXXXX|XXXXXX..|XX......|....XX..|XX......|        |....XX..|..XX....|..XX....|....XX..|
// |........|....XX..|XX......|....XX..|XX......|        |..XXXXXX|XXXXXX..|..XXXXXX|XXXX....|
// |XXXXXXXX|XXXXXX..|XXXXXXXX|XXXXXX..|XXXXXXXX|XXXXXX..|XXXX....|....XXXX|..XX....|....XXXX|
// (4 lines)
//                   |XXXXXXXX|XXXXXX..|XXXXXXXX|XXXXXX..|XX......|......XX
//                   |XX......|........|XX......|....XX..|..XXXX..|..XXXX..
//                   |XXXXXXXX|XX......|XX......|....XX..|......XX|XX......
//                   |XX......|        |XX......|....XX..|..XXXX..|..XXXX..
//                   |XX......|        |XXXXXXXX|XXXXXX..|XX......|......XX

const unsigned char solarFoxLabelSprite[] = {
	(unsigned char) -5, // dx
	(unsigned char) +0, // dy
	5, // qty
	0b11111111,
	0b11000000,
	0b11111111,
	0b00000000,
	0b11111111,
	(unsigned char) -4, // dx
	(unsigned char) +0, // dy
	5, // qty
	0b11111100,
	0b00000000,
	0b11111100,
	0b00001100,
	0b11111100,
	(unsigned char) -3, // dx
	(unsigned char) +0, // dy
	5, // qty
	0b11111111,
	0b11000000,
	0b11000000,
	0b11000000,
	0b11111111,
	(unsigned char) -2, // dx
	(unsigned char) +0, // dy
	5, // qty
	0b11111100,
	0b00001100,
	0b00001100,
	0b00001100,
	0b11111100,
	(unsigned char) -1, // dx
	(unsigned char) +0, // dy
	5, // qty
	0b11000000,
	0b11000000,
	0b11000000,
	0b11000000,
	0b11111111,
	(unsigned char) +0, // dx
	(unsigned char) +4, // dy
	1, // qty
	0b11111100,
	(unsigned char) +1, // dx
	(unsigned char) +0, // dy
	5, // qty
	0b00000011,
	0b00001100,
	0b00001100,
	0b00111111,
	0b11110000,
	(unsigned char) +2, // dx
	(unsigned char) +0, // dy
	5, // qty
	0b11000000,
	0b00110000,
	0b00110000,
	0b11111100,
	0b00001111,
	(unsigned char) +3, // dx
	(unsigned char) +0, // dy
	5, // qty
	0b00111111,
	0b00110000,
	0b00110000,
	0b00111111,
	0b00110000,
	(unsigned char) +4, // dx
	(unsigned char) +0, // dy
	5, // qty
	0b11111100,
	0b00001100,
	0b00001100,
	0b11110000,
	0b00001111,
	(unsigned char) -3, // dx
	(unsigned char) +9, // dy
	5, // qty
	0b11111111,
	0b11000000,
	0b11111111,
	0b11000000,
	0b11000000,
	(unsigned char) -2, // dx
	(unsigned char) +9, // dy
	3, // qty
	0b11111100,
	0b00000000,
	0b11000000,
	(unsigned char) -1, // dx
	(unsigned char) +9, // dy
	5, // qty
	0b11111111,
	0b11000000,
	0b11000000,
	0b11000000,
	0b11111111,
	(unsigned char) +0, // dx
	(unsigned char) +9, // dy
	5, // qty
	0b11111100,
	0b00001100,
	0b00001100,
	0b00001100,
	0b11111100,
	(unsigned char) +1, // dx
	(unsigned char) +9, // dy
	5, // qty
	0b11000000,
	0b00111100,
	0b00000011,
	0b00111100,
	0b11000000,
	(unsigned char) +2, // dx
	(unsigned char) +9, // dy
	5, // qty
	0b00000011,
	0b00111100,
	0b11000000,
	0b00111100,
	0b00000011,
	(unsigned char) -128 // End of sprite.
};

// "BALLY MIDWAY" label:
//                   |....XXXX|XX......|        <::..XXXX>
//                   |XXXX....|..XX....|        |..XX....|XX....XX|XX......|
//          |......XX|........|..XX....|        |..XX....|XX..XX..|..XX....|
//          |......XX|..XX....|XX......|        |XX....XX|....XX..|..XX....|
//          |......XX|..XX..XX|........|..XXXX..|XX....XX|..XX....|XX......|
//          |....XX..|..XX....|XX......|XX......|XX..XX..|..XX....|XX....XX|....XX..|
//          |....XX..|..XX....|..XX..XX|....XX..|XXXX....|..XX..XX|....XXXX|....XX..|
//          |....XX..|XX..XX..|..XX..XX|....XX..|XXXXXX..|..XXXX..|....XX..|..XXXX..|
//          |....XXXX|XX....XX|XX......|XXXX..XX|....XXXX|XX..XXXX|XXXXXXXX|XXXX....|
//                                                                |........|..XX....|
//                                                                |........|XX......|
//                                                                |......XX|
//
// >XXXX..XX|XX..XX..|XXXX....|XX......|XX..XXXX|XX..XX..|XX......|XXXX..XX|XX..XXXX|
// |XX..XX..|XX..XX..|XX..XX..|XX......|XX..XX..|XX..XX..|XX......|XX..XX..|XX..XX..|
// |XX..XX..|XX..XX..|XX..XX..|XX..XX..|XX..XXXX|XX....XX|        |XX..XX..|XX..XXXX|..XXXXXX|
// |XX......|XX..XX..|XX..XX..|XX..XX..|XX..XX..|XX....XX|        |XX......|XX..XX..|..XX..XX|
// |XX......|XX..XX..|XXXX....|..XXXXXX|....XX..|XX....XX|        |XX......|XX..XX..|..XXXXXX|
//                                                                                  |......XX|
//                                                                                  |....XXXX|

const unsigned char ballyMidwayLabelSprite[] = {
	(unsigned char) -4, // dx
	(unsigned char) +2, // dy
	7, // qty
	0b00000011,
	0b00000011,
	0b00000011,
	0b00001100,
	0b00001100,
	0b00001100,
	0b00001111,
	(unsigned char) -3, // dx
	(unsigned char) +0, // dy
	9, // qty
	0b00001111,
	0b11110000,
	0b00000000,
	0b00110000,
	0b00110011,
	0b00110000,
	0b00110000,
	0b11001100,
	0b11000011,
	(unsigned char) -2, // dx
	(unsigned char) +0, // dy
	9, // qty
	0b11000000,
	0b00110000,
	0b00110000,
	0b11000000,
	0b00000000,
	0b11000000,
	0b00110011,
	0b00110011,
	0b11000000,
	(unsigned char) -1, // dx
	(unsigned char) +4, // dy
	5, // qty
	0b00111100,
	0b11000000,
	0b00001100,
	0b00001100,
	0b11110011,
	(unsigned char) +0, // dx
	(unsigned char) +0, // dy
	9, // qty
	0b00001111,
	0b00110000,
	0b00110000,
	0b11000011,
	0b11000011,
	0b11001100,
	0b11110000,
	0b11111100,
	0b00001111,
	(unsigned char) +1, // dx
	(unsigned char) +1, // dy
	8, // qty
	0b11000011,
	0b11001100,
	0b00001100,
	0b00110000,
	0b00110000,
	0b00110011,
	0b00111100,
	0b11001111,
	(unsigned char) +2, // dx
	(unsigned char) +1, // dy
	11, // qty
	0b11000000,
	0b00110000,
	0b00110000,
	0b11000000,
	0b11000011,
	0b00001111,
	0b00001100,
	0b11111111,
	0b00000000,
	0b00000000,
	0b00000011,
	(unsigned char) +3, // dx
	(unsigned char) +5, // dy
	6, // qty
	0b00001100,
	0b00001100,
	0b00111100,
	0b11110000,
	0b00110000,
	0b11000000,
	(unsigned char) -5, // dx
	(unsigned char) +13, // dy
	5, // qty
	0b11110011,
	0b11001100,
	0b11001100,
	0b11000000,
	0b11000000,
	(unsigned char) -4, // dx
	(unsigned char) +13, // dy
	5, // qty
	0b11001100,
	0b11001100,
	0b11001100,
	0b11001100,
	0b11001100,
	(unsigned char) -3, // dx
	(unsigned char) +13, // dy
	5, // qty
	0b11110000,
	0b11001100,
	0b11001100,
	0b11001100,
	0b11110000,
	(unsigned char) -2, // dx
	(unsigned char) +13, // dy
	5, // qty
	0b11000000,
	0b11000000,
	0b11001100,
	0b11001100,
	0b00111111,
	(unsigned char) -1, // dx
	(unsigned char) +13, // dy
	5, // qty
	0b11001111,
	0b11001100,
	0b11001111,
	0b11001100,
	0b00001100,
	(unsigned char) +0, // dx
	(unsigned char) +13, // dy
	5, // qty
	0b11001100,
	0b11001100,
	0b11000011,
	0b11000011,
	0b11000011,
	(unsigned char) +1, // dx
	(unsigned char) +13, // dy
	2, // qty
	0b11000000,
	0b11000000,
	(unsigned char) +2, // dx
	(unsigned char) +13, // dy
	5, // qty
	0b11110011,
	0b11001100,
	0b11001100,
	0b11000000,
	0b11000000,
	(unsigned char) +3, // dx
	(unsigned char) +13, // dy
	5, // qty
	0b11001111,
	0b11001100,
	0b11001111,
	0b11001100,
	0b11001100,
	(unsigned char) +4, // dx
	(unsigned char) +15, // dy
	5, // qty
	0b00111111,
	0b00110011,
	0b00111111,
	0b00000011,
	0b00001111,
	(unsigned char) -128 // End of sprite.
};

// "COPYRIGHT" label:
// >......XX|XXXXXX..|
// |....XX..|......XX|        |XX..XXXX|XX..XXXX<@@..XX..>....XX..|XXXXXX..|XXXXXX..|XXXXXX..|
// |..XX....|XXXX....|XX......|XX..XX..|XX..XX..|XX..XX..|....XX..|XX..XX..|XX..XX..|....XX..|
// |..XX..XX|........|XX......|XX..XXXX|XX..XXXX|XX..XX..|....XX..|XXXXXX..|XXXXXX..|XXXXXX..|
// |..XX..XX|........|XX......|XX......|XX..XX..|XX..XX..|....XX..|....XX..|XX..XX..|....XX..|
// |..XX....|XXXX....|XX......|XX..XXXX|XX..XXXX|XX..XX..|XX..XX..|XXXXXX..|XXXXXX..|XXXXXX..|
// |....XX..|......XX|                                   |XX......|
// |......XX|XXXXXX..|

const unsigned char copyrightLabelSprite[] = {
	(unsigned char) -5, // dx
	(unsigned char) -1, // dy
	8, // qty
	0b00000011,
	0b00001100,
	0b00110000,
	0b00110011,
	0b00110011,
	0b00110000,
	0b00001100,
	0b00000011,
	(unsigned char) -4, // dx
	(unsigned char) -1, // dy
	8, // qty
	0b11111100,
	0b00000011,
	0b11110000,
	0b00000000,
	0b00000000,
	0b11110000,
	0b00000011,
	0b11111100,
	(unsigned char) -3, // dx
	(unsigned char) +1, // dy
	4, // qty
	0b11000000,
	0b11000000,
	0b11000000,
	0b11000000,
	(unsigned char) -2, // dx
	(unsigned char) +0, // dy
	5, // qty
	0b11001111,
	0b11001100,
	0b11001111,
	0b11000000,
	0b11001111,
	(unsigned char) -1, // dx
	(unsigned char) +0, // dy
	5, // qty
	0b11001111,
	0b11001100,
	0b11001111,
	0b11001100,
	0b11001111,
	(unsigned char) +0, // dx
	(unsigned char) +0, // dy
	5, // qty
	0b11001100,
	0b11001100,
	0b11001100,
	0b11001100,
	0b11001100,
	(unsigned char) +1, // dx
	(unsigned char) +0, // dy
	6, // qty
	0b00001100,
	0b00001100,
	0b00001100,
	0b00001100,
	0b11001100,
	0b11000000,
	(unsigned char) +2, // dx
	(unsigned char) +0, // dy
	5, // qty
	0b11111100,
	0b11001100,
	0b11111100,
	0b00001100,
	0b11111100,
	(unsigned char) +3, // dx
	(unsigned char) +0, // dy
	5, // qty
	0b11111100,
	0b11001100,
	0b11111100,
	0b11001100,
	0b11111100,
	(unsigned char) +4, // dx
	(unsigned char) +0, // dy
	5, // qty
	0b11111100,
	0b00001100,
	0b11111100,
	0b00001100,
	0b11111100,
	(unsigned char) -128 // End of sprite.
};

const unsigned char* digitSprite[] = {
	digit0Sprite,
	digit1Sprite,
	digit2Sprite,
	digit3Sprite,
	digit4Sprite,
	digit5Sprite,
	digit6Sprite,
	digit7Sprite,
	digit8Sprite,
	digit9Sprite
};


void drawSprite(unsigned char x0, unsigned char y0, unsigned char* sprite, unsigned char fgColorByte, unsigned char bgColorByte, enum operacoes operation) {
	static unsigned char x, y;
	static signed char dx;
	static unsigned char bytesQty;
	static unsigned char* byteAtVRAM;

	MC1000_ATIVA_VRAM(MC1000_MODBUF);

	while ((dx = *((signed char*) sprite++)) != -128) {
		x = x0 + dx;
		y = y0 + *((signed char*) sprite++);
		byteAtVRAM = MC1000_VRAM + x + 32 * y;
		for (bytesQty = *(sprite++); bytesQty > 0; bytesQty--) {

			*byteAtVRAM =
				(operation == COR_FUNDO) ? bgColorByte :
				(operation == BRUTO) ? *sprite :
				(operation == COR_FRENTE_E_TRANSPARENCIA) ? (bgColorByte & ~*sprite) | (*sprite & fgColorByte) :
				/* ERRO */ 0xff;

			sprite++;
			byteAtVRAM += 32;
		}
	}

	MC1000_INATIVA_VRAM(MC1000_MODBUF);
}
