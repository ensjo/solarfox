enum operacoes {
	COR_FUNDO, // Preenche o bloco apenas com o byte de cor de fundo.
	BRUTO, // Preenche o bloco com os bytes brutos da figura, sem transparência.
	COR_FRENTE_E_TRANSPARENCIA // Preenche o bloco com cor de frente, usando o byte da figura como máscara.
};

void limpaBuffer();
//void insereFigura(unsigned char x0, unsigned char y0, unsigned char* figura, unsigned char byteCorFrente, unsigned char byteCorFundo, enum operacoes operacao);
void insereFigura(unsigned char, unsigned char, unsigned char*, unsigned char, unsigned char, enum operacoes);
void desenhaBuffer();