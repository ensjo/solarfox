#include "../mc1000/mc1000.h"
#include "bufferdevideo.h"

// ----

// O buffer de tela consiste em 32 listas ligadas (linked lists)
// com o conteúdo a ser desenhado em cada coluna da tela.

// Exemplo: Dois blocos de bytes na coluna 1.

//                           0   1   2          31
//                         +---+---+---+-     -+---+
// Buffer.listasPorColuna: | 0 | @ | 0 |  ...  | 0 |
//                         +---+-|-+---+-     -+---+
//                               |
// Buffer.topo: @----------------------------------------+
//                               |                       |
//                        +------+                       |
//                        |  +----------------------+    |
// Buffer.primeiroBloco:  +->|yIni: 23              |    |
//                           |yApos: 30             |    |
//                           |proximoBloco: @---------+  |
//                           |primeiroByte: 00011000| |  |
//                           |              00100100| |  |
//                           |              01000010| |  |
//                           |              10000001| |  |
//                           |              01000010| |  |
//                           |              00100100| |  |
//                           |              00011000| |  |
//                           +----------------------+ |  |
//                        +---------------------------+  |
//                        |  +----------------------+    |
//                        +->|yIni: 98              |    |
//                           |yApos: 100            |    |
//                           |proximoBloco: 0       |    |
//                           |primeiroByte: 10101010|    |
//                           |              01010101|    |
//                           +----------------------+    |
//                        +------------------------------+
//                        |
//                        +->(Memória livre)


#define LINHA_FORA_DA_TELA 192

typedef struct _Bloco {
	unsigned char yIni; // A posição vertical inicial do conteúdo deste bloco.
	unsigned char yApos; // A posição vertical posterior ao conteúdo deste bloco. (y final + 1)
	struct _Bloco* proximoBloco; // Ponteiro para o bloco seguinte na lista.
	unsigned char primeiroByte; // Posição a partir de onde o conteúdo do bloco (bytes) são armazenados.
} Bloco;

__at 0x6800
//extern
struct {
	Bloco* listasPorColuna[32]; // 32 ponteiros, os inícios das 32 listas ligadas correspondentes a cada coluna da tela.
	unsigned char* topo; // Ponteiro para a posição onde o próximo bloco será criado. (Inicialmente aponta para Buffer->primeiroBloco.)
	Bloco primeiroBloco; // Posição a partir de onde os blocos são criados.
} Buffer;


// Reinicia o buffer:

//                           0   1   2          31
//                         +---+---+---+-     -+---+
// Buffer.listasPorColuna: | 0 | 0 | 0 |  ...  | 0 |
//                         +---+-+-+---+-     -+---+
// Buffer.topo: @---------+
//                        |
// Buffer.primeiroBloco:  +->(Memória livre)


void limpaBuffer() {
	Bloco** p;

	Buffer.topo = (unsigned char*) &(Buffer.primeiroBloco);
	for (p = Buffer.listasPorColuna; p < Buffer.listasPorColuna + 32; ) {
		*p++ = 0;
	}
}

static void insereBytes(unsigned char x, unsigned char yIni, unsigned char yApos, unsigned char* bytes, unsigned char byteCorFrente, unsigned char byteCorFundo, enum operacoes operacao) {

	static Bloco** ptrBlocoAtual; // Ponteiro para o ponteiro do início da lista ou do bloco anterior que aponta para o bloco atual.
	static Bloco* blocoAtual; // Ponteiro para o bloco atual.
	static Bloco* novoBloco; // Ponteiro para um novo bloco sendo inserido na lista.
	static unsigned char y;
	static unsigned char menorYApos;
	static unsigned char* byteAtual;
	
	if (yIni >= LINHA_FORA_DA_TELA) return;
	
	if (yApos > LINHA_FORA_DA_TELA) yApos = LINHA_FORA_DA_TELA;
	
	ptrBlocoAtual = &(Buffer.listasPorColuna[x]);
	blocoAtual = *ptrBlocoAtual;

	y = yIni;
	
	while (y < yApos) {

		// Localiza ou insere o bloco a escrever.

		novoBloco = 0;

		while (blocoAtual && y >= blocoAtual->yApos) {
		
			// Avança para o próximo bloco, se houver.

			ptrBlocoAtual = &(blocoAtual->proximoBloco);
			blocoAtual = blocoAtual->proximoBloco;

		}

		if (blocoAtual) {

			if (y < blocoAtual->yIni) {

				// Insere novo bloco antes do bloco atual.

				novoBloco = (Bloco*) Buffer.topo;
				novoBloco->yIni = y;
				novoBloco->yApos = (yApos <= blocoAtual->yIni) ? yApos : blocoAtual->yIni; // O novo bloco não pode coincidir com o bloco atual.
				novoBloco->proximoBloco = blocoAtual;

				byteAtual = &(novoBloco->primeiroByte);
				Buffer.topo = byteAtual + (novoBloco->yApos - novoBloco->yIni);
				
				// O bloco recém criado passa a ser o atual.

				blocoAtual = novoBloco;
				*ptrBlocoAtual = novoBloco;

			} else {
			
				// Usa o bloco encontrado.

				byteAtual = &(blocoAtual->primeiroByte) + (y - blocoAtual->yIni);

			}

		} else {

			// Fim da lista atingido. Insere novo bloco.
			
			novoBloco = (Bloco*) Buffer.topo;
			novoBloco->yIni = y;
			novoBloco->yApos = yApos;
			novoBloco->proximoBloco = 0;

			byteAtual = &(novoBloco->primeiroByte);
			Buffer.topo = byteAtual + (novoBloco->yApos - novoBloco->yIni);
			
			// O bloco recém criado passa a ser o atual.

			*ptrBlocoAtual = novoBloco;
			ptrBlocoAtual = &(novoBloco->proximoBloco);
			blocoAtual = novoBloco;

		}
		
		// Insere bytes no bloco.

		menorYApos = yApos <= blocoAtual->yApos ? yApos : blocoAtual->yApos;

		while (y < menorYApos) {

			*byteAtual =
				(operacao == COR_FUNDO) ? byteCorFundo :
				(operacao == BRUTO) ? *bytes :
				(operacao == COR_FRENTE_E_TRANSPARENCIA) ? ((novoBloco ? byteCorFundo : *byteAtual) & ~*bytes) | (*bytes & byteCorFrente) :
				/* ERRO */ 0xff;

			byteAtual++;
			bytes++;
			y++;
		
		}
	}
}

// Insere uma figura no buffer.

// A figura é desenhada com relação a uma coordenada x0, y0.
// A coordenada horizontal de refere a byte (0~31), não a pixel.
// O parâmetro "figura" consiste em uma sequência de "blocos" de transferência:
// - deslocamento horizontal em relação a x0 (se for -128, indica o fim da figura).
// - deslocamento vertical em relação a y0.
// - quantidade de bytes a transferir na vertical.
// - sequência de bytes.

// Exemplo: Considere-se esta figura, com o sinal de ">" indicando a posição da
// coordenada de referência x0, y0:

// |........|..XXXX..|........
// |......XX|XXXXXXXX|XX......
// |......XX>XX....XX|XX......
// |......XX|XX....XX|XX......
// |......XX|XXXXXXXX|XX......
// |........|..XXXX..|........

// A sequência de bytes que representaria esta figura seria:

// (dx) -1, (dy) -1, (quantidade de bytes) 4,
// (4 bytes na vertical) 0b00000011, 0b00000011, 0b00000011, 0b00000011,
// (dx)  0, (dy) -2, (quantidade de bytes) 6,
// (6 bytes na vertical) 0b00111100, 0b11111111, 0b11000011, 0b11000011, 0b11000011, 0b11111111, 0b00111100,
// (dx) +1, (dy) -1, (quantidade de bytes) 4,
// (4 bytes na vertical) 0b11000000, 0b11000000, 0b11000000, 0b11000000,
// (dx) -128 (fim da figura)

// Os bytes efetivamente inseridos no buffer dependem do parâmetro "operação":

// BRUTO: Insere os bytes da figura no buffer.
// FUNDO: Apenas segue a forma da figura, mas insere sempre o valor do
//        parâmetro byteCorFundo. Serve para apagar.
// COR_FRETE_E_TRANSPARENCIA: Usa os bytes da figura como uma máscara.
//        Os bits "1" são substituídos pelos bits correspondentes do parâmetro
//        byteCorFrente.
//        Os bits "0" são substituídos pelos bits correspondentes do que já
//        estiver armazenado no buffer, ou se ainda não houver nada no buffer
//        nessa posição, do parâmetro byteCorFundo.

void insereFigura(unsigned char x0, unsigned char y0, unsigned char* figura, unsigned char byteCorFrente, unsigned char byteCorFundo, enum operacoes operacao) {
	static unsigned char x, y;
	static signed char dx;
	static unsigned char qtdBytes;

	while ((dx = *((signed char*) figura++)) != -128) {
		x = x0 + dx;
		y = y0 + *((signed char*) figura++);
		qtdBytes = *(figura++);
		insereBytes(x, y, y + qtdBytes, figura, byteCorFrente, byteCorFundo, operacao);
		figura += qtdBytes;
	}
}

// Transfere para a VRAM o conteúdo do buffer de tela.

void desenhaBuffer() __naked {
__asm
;	Bloco** lista;
;	Bloco* bloco;
;	unsigned char* byte;
;	unsigned char qtdColunasATransferir;
;	unsigned char qtdBytesATransferir;
;	unsigned char* inicioDaColunaNaVRAM;
;	unsigned char* pontoDeEscritaNaVRAM;

;	lista = Buffer.listasPorColuna;
;	inicioDaColunaNaVRAM = MC1000_VRAM;
;	for (qtdColunasATransferir = 32; qtdColunasATransferir > 0; qtdColunasATransferir--) {
;		bloco = *(lista++);
;		while (bloco) {
;			pontoDeEscritaNaVRAM = inicioDaColunaNaVRAM + 32 * bloco->yIni;
;			byte = &(bloco->primeiroByte);
;			for (qtdBytesATransferir = bloco->yApos - bloco->yIni; qtdBytesATransferir > 0; qtdBytesATransferir--) {
;				*pontoDeEscritaNaVRAM = *(byte++);
;				pontoDeEscritaNaVRAM += 32;
;			}
;			bloco = bloco->proximoBloco;
;		}
;	}
;
	.z180
	.allow_undocumented

	push	ix ; Empilha IX e IY, que podem estar sendo usados pela rotina chamadora.
	push	iy

	ld	hl,#_Buffer+0 ; Ponteiro para a lista correspondente à primeira coluna.

	ld	bc,#_MC1000_VRAM ; Ponteiro para o início da primeira coluna na VRAM.

	ld	ixh,#32 ; Quantidade de colunas a transferir.

	ld	a,(_MC1000_MODBUF) ; Obtém valor de modo de vídeo a usar na porta COL32.
	and	a,#0b11111110 ; Bit 0=0 indica VRAM ativa.
	ld	iyh,a
	inc	a ; Bit 1=1 indica VRAM inativa.
	ld	iyl,a

lacoColunas:
	ld	e,(hl) ; Obtém ponteiro para o primeiro bloco da lista.
	inc	hl
	ld	d,(hl)
	inc	hl

	push	hl ; Empilha ponteiro para a lista correspondente à próxima coluna.

lacoBlocos:
	ld	a,e ; Sai do laço se não houver mais blocos a transferir.
	or	d
	jp	z,fimLacoBlocos

	push	bc ; Empilha ponteiro para início da coluna na VRAM.

	ld	a,(de) ; Obtém coordenada y inicial do bloco.
	inc	de

	ld	l,a ; Calcula ponto de escrita na VRAM para o primeiro byte do bloco.
	ld	h,#0
	add	hl,hl
	add	hl,hl
	add	hl,hl
	add	hl,hl
	add	hl,hl
	add	hl,bc

	push	hl ; Empilha ponto de escrita na VRAM para o primeiro byte do bloco.

	ld	b,a

	ld	a,(de) ; Obtém coordenada y após o bloco.
	inc	de

	sub	a,b ; Calcula quantidade de bytes a transferir.
	ld	ixl,a

	ld	a,(de) ; Obtém ponteiro para o próximo bloco.
	inc	de
	ld	l,a
	ld	a,(de)
	inc	de
	ld	h,a

	ex	(sp),hl ; Desempilha ponto na escrita na VRAM para o primeiro byte do bloco e empilha ponteiro para o próximo bloco.

	ld	bc,#32

	ld	a,iyh ; Ativa VRAM.
	out	(_MC1000_COL32),a

lacoBytes:
	ld	a,(de) ; Obtém byte atual do bloco.
	inc	de ; Avança ponteiro para próximo byte.
	ld	(hl),a ; Armazena byte no ponto de escrita na VRAM.

	add	hl,bc ; Avança ponto de escrita na VRAM para a próxima linha (+32).

	dec	ixl ; Sai do laço se não houver mais bytes a transferir.
	jp	nz,lacoBytes

fimLacoBytes:
	ld	a,iyl ; Inativa VRAM.
	out	(_MC1000_COL32),a

	pop	de ; Desempilha ponteiro para o próximo bloco.
	pop	bc ; Desempilha ponteiro para o início da coluna na VRAM.

	jp	lacoBlocos

fimLacoBlocos:
	pop	hl ; Desempilha ponteiro para a próxima coluna.

	inc	bc ; Avança ponteiro para a próxima coluna na VRAM.

	dec	ixh ; Sai do laço se não houver mais colunas a transferir.
	jp	nz,lacoColunas

fimLacoColunas:
	pop	iy ; Desempilha IX e IY.
	pop	ix

	ret
__endasm;
}
